﻿namespace UniversityLaboratory2._4
{
    class Program
    {
        private static void Main()
        {
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                new LaboratoryInfo(
                    "Бродский Егор Станиславович",
                    "ИР-133Б",
                    4,
                    "Управляючі оператори для організації циклів."
                )
            );
            Console.WriteLine();
            TaskChooser.Run(new TaskInfo[]
            {
                new TaskInfo("Завдання 1", () =>
                {
                    const double xStep = 0.02;
                    const int xMin = 0;
                    const double xMax = 0.5;
                    const int range = (int) ((xMax - xMin) / xStep);
                    var numberArray =
                        (new double[] {15, 30, 45, 60}).Distinct()
                        .ToArray();
                    var xArray = Enumerable.Range(0, range).Select(i => xMin + i * xStep).ToArray();

                    foreach (var a in numberArray)
                    {
                        Console.WriteLine($"Значення в точках при a = {a}");
                        foreach (var x in xArray)
                        {
                            Console.WriteLine(
                                $"y({x}) = {x * Math.Tan(a) - Math.Pow(x, 2) / Math.Pow(Math.Cos(a), 2)}"
                            );
                        }
                    }

                    return null;
                }),
                new TaskInfo("Завдання 2", () =>
                {
                    var xArray = new int[] {2, 1, 0};
                    var yArray = xArray.Select(x => 0.5 * Math.Sqrt(x)).ToArray();
                    Console.WriteLine("Контрольні значення:");
                    foreach (var (value, i) in yArray.Select((value, i) => ( value, i )))
                    {
                        Console.WriteLine($"y({xArray[i]}) = {value}");
                    }

                    Console.WriteLine("Актуальный:");
                    foreach (var x in xArray)
                    {
                        var isCorrect = false;
                        double sum = 0;
                        const int startN = 2;
                        var n = startN;
                        while (!isCorrect)
                        {
                            sum += Math.Pow(-1, n - 1) *
                                Enumerable.Range(startN, n).Select(i => (2 * i - 3))
                                    .Aggregate((acc, num) => acc * num) / Enumerable.Range(1, Math.Max(1, n))
                                    .Aggregate((acc, num) => acc * num) * Math.Pow(2, 3 * n) * Math.Pow(x-4, n);
                            
                            n++;
                            
                            if(n > 10) isCorrect = true;
                        }
                        
                        Console.WriteLine($"y({x}) = {sum}");
                    }

                    return null;
                }),
            });
        }
    }
}