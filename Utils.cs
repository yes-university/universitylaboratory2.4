﻿namespace UniversityLaboratory2._4;

public static class Utils
{
    public static int ToInt(string? text)
    {
        var minus = false;
        var parsed = int.Parse(
            string.Join("",
                (new string[] {"0"})
                .Concat(
                    text?.ToCharArray()
                        .Select((c) =>
                        {
                            var str = c.ToString();
                            if (str != "-") return str;
                            minus = true;
                            return "0";
                        })
                        .Where((s =>
                                {
                                    try
                                    {
                                        int.Parse(s);
                                        return true;
                                    }
                                    catch (Exception e)
                                    {
                                        return false;
                                    }
                                }
                            ))
                    ?? Array.Empty<string>()
                )
            )
        );
        return minus ? -parsed : parsed;
    }

    public static float ToFloatInt(string? text)
    {
        return float.Parse(text ?? "0");
    }

    public static double ToDoubleInt(string? text)
    {
        return Convert.ToDouble(text ?? "0");
    }
}